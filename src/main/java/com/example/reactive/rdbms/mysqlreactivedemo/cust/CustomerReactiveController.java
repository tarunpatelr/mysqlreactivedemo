package com.example.reactive.rdbms.mysqlreactivedemo.cust;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_STREAM_JSON_VALUE;

@RestController
@Log4j2
@RequestMapping("/reactive")
public class CustomerReactiveController {

    @Autowired
    private CustomerReactiveRepository customerReactiveRepository;

    @GetMapping(path = "/customers", produces = APPLICATION_STREAM_JSON_VALUE)
    public Flux<Customer> getNonBlockingCustomers() {

        log.info("fetching all customers in blocking way. ");
        return timeLapseRecorder.apply(customerReactiveRepository::findAll);
    }

    @GetMapping(path = "/delayed/customers", produces = APPLICATION_STREAM_JSON_VALUE)
    public Flux<Customer> getBlockingCustomerWithDelay(@RequestParam(name = "delay") Float delay) {

        log.info("fetching all customers in blocking way with delay " + delay);
        return timeLapseRecorder.apply(() -> customerReactiveRepository.getCustomersWithDelay(delay));
    }

    private static Function<Supplier<Flux<Customer>>, Flux<Customer>> timeLapseRecorder = customerSupplier -> {

        Instant start = Instant.now();
        Flux<Customer> customers = customerSupplier.get();
        Instant finish = Instant.now();
        log.info("time taken in ms :  " + Duration.between(start, finish).toMillis());
        return customers;
    };
}
